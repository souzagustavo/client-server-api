package main

import (
	"context"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"
	"text/template"
	"time"
)

type Cotacao struct {
	Bid string `json:"bid"`
}

type Client struct {
	timeout    time.Duration //ms
	outputFile string        //cotacao.txt
}

func (c *Client) GetCotacao() (*Cotacao, error) {
	ctx, cancel := context.WithTimeout(context.Background(), c.timeout*time.Millisecond)
	defer cancel()

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "http://localhost:8080/cotacao", nil)
	if err != nil {
		return nil, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	var cotacao Cotacao
	err = json.Unmarshal(body, &cotacao)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return &cotacao, nil
}

func (c *Client) SaveBid(cotacao Cotacao) error {
	file, err := os.Create(c.outputFile)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	defer file.Close()

	log.Printf("Dólar: R$%s\n", cotacao.Bid)

	t := template.Must(template.New("cotacao").Parse("Dólar: {{.Bid}}"))
	err = t.Execute(file, cotacao)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	return nil
}
