package main

import (
	"fmt"
	"os"
	"testing"
)

const (
	FileCotacaoTest = "cotacao-teste.txt"
)

func TestClient(t *testing.T) {

	//teste forçando estourar timeout no contexto ( timeout 1ms)
	t.Run("Teste estourando timeout ctx", func(t *testing.T) {
		c := Client{timeout: 1}

		_, err := c.GetCotacao()
		if err == nil {
			t.Error("Deveria retornar um erro, mas err é nil")
			return
		}
		if !os.IsTimeout(err) {
			t.Errorf("Deveria ser um erro de timeout (err:%v)", err)
			return
		}
	})

	//teste que verifica criação de arquivo (ao final arquivo de teste é removido)
	t.Run("verificando criação do arquivo cotacao-teste.txt", func(t *testing.T) {
		c := Client{timeout: 300, outputFile: FileCotacaoTest}

		cotacao := Cotacao{Bid: "10.50"}

		err := c.SaveBid(cotacao)
		if err != nil {
			t.Error(err.Error())
			return
		}

		dados, err := os.ReadFile(FileCotacaoTest)
		if err != nil {
			t.Error(err.Error())
			return
		}

		if dados == nil || len(dados) == 0 {
			t.Error("Arquivo não deveria estar vazio.")
			return
		}

		if string(dados) != fmt.Sprintf("Dólar: %s", cotacao.Bid) {
			t.Error("Conteúdo do arquivo não seguiu valor do template.")
			return
		}

		if err = os.Remove(FileCotacaoTest); err != nil {
			t.Log(err.Error())
		}

	})
}
