package main

func main() {
	c := Client{timeout: 300, outputFile: "cotacao.txt"}

	cotacao, err := c.GetCotacao()
	if err != nil {
		panic(err)
	}

	err = c.SaveBid(*cotacao)
	if err != nil {
		panic(err)
	}
}
