package main

import (
	"github.com/glebarez/sqlite"
	"gorm.io/gorm"

	"context"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"
)

type Cotacao struct {
	ID         int    `json:"-" gorm:"primaryKey"`
	Code       string `json:"code"`
	Codein     string `json:"codein"`
	Name       string `json:"name"`
	High       string `json:"high"`
	Low        string `json:"low"`
	VarBid     string `json:"varBid"`
	PctChange  string `json:"pctChange"`
	Bid        string `json:"bid"`
	Ask        string `json:"ask"`
	Timestamp  string `json:"timestamp"`
	CreateDate string `json:"create_date"`
}

func (Cotacao) TableName() string {
	return "cotacoes"
}

// json resposta https://economia.awesomeapi.com.br/json/last/USD-BRL
type Resposta struct {
	Cotacao Cotacao `json:"USDBRL"`
}

// json que será retornado pelo handler de GET /cotacao
type CotacaoDTO struct {
	Bid string `json:"bid"`
}

type Server struct {
	timeoutHTTP time.Duration //timeout para requisições http em milisegundos
	timeoutDB   time.Duration //timeout para banco de dados em milisegundos
	db          *gorm.DB      //conexão com o banco (automaticamente iniciada chamando método Listen())
}

func (s *Server) GetCotacao() (*Cotacao, error) {
	//cria contexto com timeout em milisegundos para requisição
	ctx, cancel := context.WithTimeout(context.Background(), s.timeoutHTTP*time.Millisecond)
	defer cancel()

	//cria requisição passando contexto com timeout
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "https://economia.awesomeapi.com.br/json/last/USD-BRL", nil)
	if err != nil {
		return nil, err
	}
	//executa requisição
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	defer resp.Body.Close()

	//leitura dos bytes da resposta
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	//realiza parse dos bytes para struct
	var resposta Resposta
	err = json.Unmarshal(body, &resposta)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	//retorna objeto de cotação
	return &resposta.Cotacao, nil
}

func (s *Server) SaveCotacao(cotacao Cotacao) error {
	//cria contexto com timeout em milisegundos para executar insert no banco
	ctx, cancel := context.WithTimeout(context.Background(), s.timeoutDB*time.Millisecond)
	defer cancel()

	//realiza insert da cotação com timeout
	err := s.db.WithContext(ctx).Create(&cotacao).Error
	if err != nil {
		log.Println(err.Error())
	}
	return err
}

func (s *Server) Listen() {
	log.Println("Iniciando sqlite")

	db, err := gorm.Open(sqlite.Open("banco.db"), &gorm.Config{})
	if err != nil {
		log.Fatal(err.Error())
	}

	log.Println("Inicializando migração tabelas")
	err = db.AutoMigrate(&Cotacao{})
	if err != nil {
		log.Fatal(err.Error())
	}

	s.db = db

	log.Println("Iniciando mux & listenAndServe")

	routes := http.NewServeMux()
	routes.HandleFunc("/cotacao", s.CotacaoHandler)

	log.Fatal(http.ListenAndServe(":8080", routes))
}

func (s *Server) CotacaoHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Iniciando solicitação de cotação")
	cotacao, err := s.GetCotacao()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	log.Println("Salvando cotação no banco")
	err = s.SaveCotacao(*cotacao)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Println("Retornando cotação para client.")
	cotacaoDTO := CotacaoDTO{cotacao.Bid}

	_ = json.NewEncoder(w).Encode(cotacaoDTO)
}
