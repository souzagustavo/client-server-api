package main

import (
	"os"
	"testing"
)

func TestServer(t *testing.T) {

	t.Run("Teste estourando timeout ctx requisição http para economia", func(t *testing.T) {
		s := Server{timeoutHTTP: 1, timeoutDB: 10}
		_, err := s.GetCotacao()
		if err == nil {
			t.Error("Deveria retornar um erro, mas err é nil")
			return
		}
		if !os.IsTimeout(err) {
			t.Errorf("Deveria ser um erro de timeout (err:%v)", err)
			return
		}
	})

	t.Run("Teste obtendo bid", func(t *testing.T) {
		s := Server{timeoutHTTP: 200, timeoutDB: 10}
		cotacao, err := s.GetCotacao()
		if err != nil {
			t.Errorf("Erro não esperado (err:%v)", err)
			return
		}

		if cotacao.Bid == "" {
			t.Errorf("Cotacao.bid deveria estar preenchido")
			return
		}

	})
}
